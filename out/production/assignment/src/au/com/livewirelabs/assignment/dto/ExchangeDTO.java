package au.com.livewirelabs.assignment.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExchangeDTO {

    private String[] codeList;
    private Map<String, Integer> stockUnitsOwned;
    private Map<String, Double> incomeOfExchange;
    private Map<String, Double> stockPrices;
    private List<TransactionDTO> transactions;

    public ExchangeDTO() {
        this.transactions = new ArrayList<TransactionDTO>();
    }

    public String[] getCodeList() {
        return codeList;
    }

    public void setCodeList(String[] codeList) {
        this.codeList = codeList;
    }

    public Map<String, Integer> getStockUnitsOwned() {
        return stockUnitsOwned;
    }

    public void setStockUnitsOwned(Map<String, Integer> stockUnitsOwned) {
        this.stockUnitsOwned = stockUnitsOwned;
    }

    public void setStockUnitsOwnedByCode(String code, int units) {
        this.stockUnitsOwned.put(code, units);
    }

    public Map<String, Double> getIncomeOfExchange() {
        return incomeOfExchange;
    }

    public void setIncomeOfExchange(Map<String, Double> incomeOfExchange) {
        this.incomeOfExchange = incomeOfExchange;
    }

    public void setIncomeOfExchangeByCode(String code, Double income) {
        this.incomeOfExchange.put(code, income);
    }

    public Map<String, Double> getStockPrices() {
        return stockPrices;
    }

    public void setStockPrices(Map<String, Double> stockPrices) {
        this.stockPrices = stockPrices;
    }

    public List<TransactionDTO> getTransactions() {
        return transactions;
    }
    public void setTransactions(List<TransactionDTO> transactions) {
        this.transactions = transactions;
    }
    public void addTransaction(TransactionDTO transaction) {
        this.transactions.add(transaction);
    }
}
