package au.com.livewirelabs.assignment.service;

import au.com.livewirelabs.assignment.ResourceService;
import com.google.inject.AbstractModule;

public class ResourceServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ResourceService.class).to(ResourceServiceImpl.class);
    }
}
