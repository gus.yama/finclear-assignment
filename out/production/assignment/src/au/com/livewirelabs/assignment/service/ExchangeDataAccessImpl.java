package au.com.livewirelabs.assignment.service;

import au.com.livewirelabs.assignment.ExchangeDataAccess;
import au.com.livewirelabs.assignment.dto.ExchangeDTO;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ExchangeDataAccessImpl implements ExchangeDataAccess {

    public ExchangeDTO getExchange(Boolean isAsx) throws IOException
    {
        File file = getFile(isAsx ? "asx-assignment.txt" : "cxa-assignment.txt");
        //Read File Content
        String json = new String(Files.readAllBytes(file.toPath()));
        Gson gson = new Gson();
        ExchangeDTO exchangeDTO = gson.fromJson(json, ExchangeDTO.class);
        return exchangeDTO;
    }

    public void setExchange(ExchangeDTO exchangeDTO, Boolean isAsx) throws IOException
    {
        File file = getFile(isAsx ? "asx-assignment.txt" : "cxa-assignment.txt");
        Gson gson = new Gson();
        String json = gson.toJson(exchangeDTO, ExchangeDTO.class);
        Files.write( file.toPath(), json.getBytes());
    }

    private File getFile(String fileName) {
        String documentFolder = System.getProperty("user.home") + "/Documents/";
        File file = new File(documentFolder + fileName);
        return file;
    }
}