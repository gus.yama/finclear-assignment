package au.com.livewirelabs.assignment.service;

import au.com.livewirelabs.assignment.ResourceService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class ResourceServiceImpl implements ResourceService {

    private String[] fileNames = {"asx-assignment.txt", "asx-exchange.txt", "cxa-assignment.txt", "cxa-exchange.txt"};

    public void prepareResource() throws IOException {
        String documentFolder = System.getProperty("user.home") + "/Documents/";
        for (String fileName : fileNames) {
            File file = new File(documentFolder+ fileName);
            if(!file.exists()) {
                ClassLoader classLoader = getClass().getClassLoader();
                File resourceFile = new File(classLoader.getResource(fileName).getFile());
                Files.copy(resourceFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }

    public File getFile(String fileName) {
        String documentFolder = System.getProperty("user.home") + "/Documents/";
        File file = new File(documentFolder + fileName);
        return file;
    }

}
