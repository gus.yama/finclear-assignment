package au.com.livewirelabs.assignment.asx;

import au.com.livewirelabs.assignment.*;
import au.com.livewirelabs.assignment.service.StockExchangeDataAccessModule;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class AsxStockExchangeModule extends AbstractModule {

    @Override
    protected void configure() {
        Injector injector = Guice.createInjector(new StockExchangeDataAccessModule());
        injector.getInstance(StockExchangeDataAccess.class);
        bind(StockExchange.class).to(AsxStockExchangeImpl.class);
    }
}
