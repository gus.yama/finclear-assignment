package au.com.livewirelabs.assignment.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public class StockExchangeDTO {
    private Map<String, Integer> volume;
    private BigDecimal tradingCost;

    public StockExchangeDTO() {}

    public StockExchangeDTO(Map<String, Integer> vol, BigDecimal cost) {
        this.volume = vol;
        this.tradingCost = cost;
    }

    public Map<String, Integer> getVolume() {
        return volume;
    }

    public void setVolume(Map<String, Integer> volume) {
        this.volume = volume;
    }

    public Integer getVolumeByCode(String code) {
        if(volume.containsKey(code)) {
            return volume.get(code);
        }
        return 0;
    }

    public void setVolumeByCode(String code, Integer vol) {
        volume.put(code, vol);
    }

    public BigDecimal getTradingCost() {
        return tradingCost;
    }

    public void setTradingCost(BigDecimal tradingCost) {
        this.tradingCost = tradingCost;
    }
}


