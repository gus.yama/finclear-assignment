package au.com.livewirelabs.assignment.service;

import au.com.livewirelabs.assignment.StockExchangeDataAccess;
import com.google.inject.AbstractModule;

public class StockExchangeDataAccessModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(StockExchangeDataAccess.class).to(StockExchangeDataAccessImpl.class);
    }
}
