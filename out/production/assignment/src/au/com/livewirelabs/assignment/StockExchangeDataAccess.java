package au.com.livewirelabs.assignment;

import au.com.livewirelabs.assignment.dto.StockExchangeDTO;

import java.io.IOException;

public interface StockExchangeDataAccess {
    StockExchangeDTO getStockExchange(String fileName) throws IOException;
    void setStockExchange(StockExchangeDTO stockExchangeDTO, Boolean isAsx) throws IOException;
}
