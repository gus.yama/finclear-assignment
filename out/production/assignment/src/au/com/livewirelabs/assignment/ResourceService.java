package au.com.livewirelabs.assignment;

import java.io.File;
import java.io.IOException;

public interface ResourceService {
    void prepareResource() throws IOException;
    File getFile(String fileName);
}
