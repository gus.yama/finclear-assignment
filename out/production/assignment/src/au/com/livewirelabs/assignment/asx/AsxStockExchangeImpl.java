package au.com.livewirelabs.assignment.asx;

import Exceptions.InvalidCodeException;
import au.com.livewirelabs.assignment.StockExchange;
import au.com.livewirelabs.assignment.StockExchangeDataAccess;
import au.com.livewirelabs.assignment.dto.StockExchangeDTO;
import au.com.livewirelabs.assignment.exception.InsufficientUnitsException;
import com.google.inject.Inject;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

public class AsxStockExchangeImpl implements StockExchange {

    private Map<String, Integer> volume;
    private BigDecimal tradingCost ;
    private StockExchangeDataAccess exchangeDataAccess;

    @Inject
    public AsxStockExchangeImpl(StockExchangeDataAccess seda) {
        this.exchangeDataAccess = seda;
        try {
            StockExchangeDTO dto = seda.getStockExchange("asx-exchange.txt");
            this.volume = dto.getVolume();
            this.tradingCost = dto.getTradingCost();
        }
        catch(IOException ioe) {
            System.out.println("Error loading ASX data. " + ioe.getMessage());
        }
    }

    public void buy(String code, Integer units) throws InsufficientUnitsException, InvalidCodeException {
        if(Arrays.asList(volume.keySet()).contains(code)) {
            throw new InvalidCodeException(code);
        }
        if(volume.get(code) < units) {
            throw new InsufficientUnitsException("ASX ");
        }
        volume.put(code, volume.get(code) - units);
        System.out.println("ASX => Buying " + units + " units of " + code);
    }

    public void sell(String code, Integer units) throws InvalidCodeException {
        if(Arrays.asList(volume.keySet()).contains(code)) {
            throw new InvalidCodeException(code);
        }
        volume.put(code, volume.get(code) + units);
        System.out.println("ASX => Selling " + units + " of " + code);
    }

    public Map<String, Integer> getOrderBookTotalVolume() {
        return volume;
    }

    public BigDecimal getTradingCosts() {
        return tradingCost;
    }
}
