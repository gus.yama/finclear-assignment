This is the assignment test given by Finclear



I have included a folder with the latest compiled application. 

Instructions:
1. Using your favorite console app, clone the app in your workspace
	git clone https://gus.yama@gitlab.com/gus.yama/finclear-assignment.git

2. Move the current folder to [root]\out\production\assignment>
	cd out\production\assignment

3. Run the following command: 
java -cp guice-3.0.jar;javax-inject.jar;aopalliance-.jar;gson-2.8.5.jar;. au/com/livewirelabs/assignment/Assignment -exchange ASX
java -cp guice-3.0.jar;javax-inject.jar;aopalliance-.jar;gson-2.8.5.jar;. au/com/livewirelabs/assignment/Assignment -exchange CXA



Assumptions
1. The sell/buy price of the stocks AB,CBA,QAN used were the same
2. As the trade fee changes, its value comes from a external source (text file)

Design choice
1. The use of text files to store/retrieve data o mock the database for its simplicity
2. Json format to store in the text file to keep values as object










[root]\out\production\assignment>java -cp guice-3.0.jar;javax-inject.jar;aopalliance-.jar;gson-2.8.5.jar;. au/com/livewirelabs/assignment/Assignment -exchange ASX
[root]\out\production\assignment>java -cp guice-3.0.jar;javax-inject.jar;aopalliance-.jar;gson-2.8.5.jar;. au/com/livewirelabs/assignment/Assignment -exchange CXA

