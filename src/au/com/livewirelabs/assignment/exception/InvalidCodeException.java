package Exceptions;

public class InvalidCodeException extends Exception {

    public InvalidCodeException(String errorMessage) {
        super(errorMessage + " is an invalid code for the ASX Stock Exchange");
    }
}
