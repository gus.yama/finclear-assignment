package au.com.livewirelabs.assignment.exception;


public class InsufficientUnitsException extends Exception {

    public InsufficientUnitsException(String errorMessage) {
        super("Insufficient volume in the Stock Market " + errorMessage);
    }
}
