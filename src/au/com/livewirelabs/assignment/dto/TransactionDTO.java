package au.com.livewirelabs.assignment.dto;

import java.util.Date;

public class TransactionDTO {
    private String stockMarket;
    private String code;
    private Double value;
    private Integer units;
    private Date date;
    private String transactionType;

    public TransactionDTO() {}

    public TransactionDTO(String stockMarket, String code, Double value, Integer units, Date date, String transactionType) {
        this.stockMarket = stockMarket;
        this.code= code;
        this.value = value;
        this.units = units;
        this.date = date;
        this.transactionType = transactionType;
    }

    public String getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(String stockMarket) {
        this.stockMarket = stockMarket;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
}
