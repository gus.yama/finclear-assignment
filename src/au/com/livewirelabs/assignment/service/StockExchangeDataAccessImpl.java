package au.com.livewirelabs.assignment.service;

import au.com.livewirelabs.assignment.StockExchangeDataAccess;
import au.com.livewirelabs.assignment.dto.StockExchangeDTO;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class StockExchangeDataAccessImpl implements StockExchangeDataAccess {

    public StockExchangeDTO getStockExchange(String fileName) throws IOException {
        File file = getFile(fileName);
        String json = new String(Files.readAllBytes(file.toPath()));
        Gson gson = new Gson();
        StockExchangeDTO dto = gson.fromJson(json, StockExchangeDTO.class);
        return dto;
    }

    public void setStockExchange(StockExchangeDTO stockExchangeDTO, Boolean isAsx) throws IOException {
        File file = getFile(isAsx ? "asx-exchange.txt" : "cxa-exchange.txt");
        Gson gson = new Gson();
        String json = gson.toJson(stockExchangeDTO, StockExchangeDTO.class);
        Files.write( file.toPath(), json.getBytes());
    }

    private File getFile(String fileName) {
        String documentFolder = System.getProperty("user.home") + "/Documents/";
        File file = new File(documentFolder + fileName);
        return file;
    }
}
