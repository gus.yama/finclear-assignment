package au.com.livewirelabs.assignment.service;

import au.com.livewirelabs.assignment.ExchangeDataAccess;
import com.google.inject.AbstractModule;


public class ExchangeDataAccessModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ExchangeDataAccess.class).to(ExchangeDataAccessImpl.class);
    }
}
