package au.com.livewirelabs.assignment.cxa;

import au.com.livewirelabs.assignment.service.ExchangeDataAccessImpl;
import au.com.livewirelabs.assignment.StockExchange;
import au.com.livewirelabs.assignment.service.StockExchangeDataAccessModule;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class CxaStockExchangeModule extends AbstractModule {

    @Override
    protected void configure() {
        Injector injector = Guice.createInjector(new StockExchangeDataAccessModule());
        injector.getInstance(ExchangeDataAccessImpl.class);
        bind(StockExchange.class).to(CxaStockExchangeImpl.class);
    }
}

