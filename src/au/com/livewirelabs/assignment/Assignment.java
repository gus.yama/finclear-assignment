package au.com.livewirelabs.assignment;

import au.com.livewirelabs.assignment.asx.AsxStockExchangeModule;
import au.com.livewirelabs.assignment.cxa.CxaStockExchangeModule;
import au.com.livewirelabs.assignment.dto.ExchangeDTO;
import au.com.livewirelabs.assignment.dto.StockExchangeDTO;
import au.com.livewirelabs.assignment.dto.TransactionDTO;
import au.com.livewirelabs.assignment.exception.InsufficientUnitsException;
import au.com.livewirelabs.assignment.service.ExchangeDataAccessModule;
import au.com.livewirelabs.assignment.service.ResourceServiceImpl;
import au.com.livewirelabs.assignment.service.StockExchangeDataAccessModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import java.io.IOException;
import java.util.Date;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;


public class Assignment {

    public static void main(String[] args) {
        Boolean isAsx = false;
        if(! Arrays.asList(args).contains("-exchange")) {
            System.out.println("Needs to include args '-exchange ASX' or '-exchange CXA'");
            return;
        }
        if(Arrays.asList(args).contains("ASX")) {
            isAsx = true;
        }
        else {
            if(Arrays.asList(args).contains("CXA")) {
                isAsx = false;
            }
            else {
                System.out.println("Missing the Stock Exchange 'ASX' or 'CXA'");
                return;
            }
        }

        try {
            new ResourceServiceImpl().prepareResource();
        }
        catch(IOException ioe){
            System.out.println("Error getting resource data. " + ioe.getMessage());
            return;
        }

        Injector injector = Guice.createInjector(isAsx ? new AsxStockExchangeModule() :  new CxaStockExchangeModule(),
                new ExchangeDataAccessModule(), new StockExchangeDataAccessModule());
        Assignment assignment = injector.getInstance(Assignment.class);
        assignment.trade(isAsx);
    }


    private StockExchange stockExchange;
    private ExchangeDataAccess exchangeDataAccess;
    private StockExchangeDataAccess stockExchangeDataAccess;

    @Inject
    public Assignment(StockExchange stockExchange, ExchangeDataAccess eda, StockExchangeDataAccess sedc) {
        this.stockExchange = stockExchange;
        this.exchangeDataAccess = eda;
        this.stockExchangeDataAccess = sedc;
    }

    public void trade(Boolean isAsx) {
        ExchangeDTO exchangeDTO;
        try {
            exchangeDTO = exchangeDataAccess.getExchange(isAsx);
        }
        catch(IOException ioe) {
            System.out.println("Error loading data: " + ioe.getMessage());
            return;
        }

        Random random = new Random();
        System.out.println("Total Volume");
        reportVolume();

        for(int i=0; i<random.nextInt(5); i++) {
            try {
                String buyCode = exchangeDTO.getCodeList()[random.nextInt(3)];
                String sellCode = exchangeDTO.getCodeList()[random.nextInt(3)];
                int buyUnits = random.nextInt(50);
                int sellUnits = random.nextInt(20);

                stockExchange.buy(buyCode, buyUnits);
                calculateBuying(isAsx, buyCode, buyUnits, exchangeDTO);
                if(exchangeDTO.getStockUnitsOwned().get(sellCode) < sellUnits) {
                    System.out.println("Insufficient stock units to sell. You owned " + exchangeDTO.getStockUnitsOwned().get(sellCode) +
                            " and am trying to sell " + sellUnits + " of code " + buyCode);
                }
                else {
                    stockExchange.sell(sellCode, sellUnits);
                    calculateSelling(isAsx, sellCode, sellUnits, exchangeDTO);
                }
            }
            catch(Exceptions.InvalidCodeException ice) {
                System.out.println(ice.getMessage());
            }
            catch(InsufficientUnitsException iue) {
                System.out.println(iue.getMessage());
            }
        }
        System.out.println("Total Volume after trading");
        reportVolume();
        reportIncomeAndUnits(exchangeDTO);
        try {
            exchangeDataAccess.setExchange(exchangeDTO, isAsx);
            StockExchangeDTO stockExchangeDTO = new StockExchangeDTO(stockExchange.getOrderBookTotalVolume(), stockExchange.getTradingCosts());
            stockExchangeDataAccess.setStockExchange(stockExchangeDTO, isAsx);
        }
        catch(IOException ioe) {
            System.out.println("Error saving data: " + ioe.getMessage());
            return;
        }
    }

    private void calculateBuying(Boolean isAsx, String code, int units, ExchangeDTO exchangeDTO) {
        Double value = calculateIncome(true, code, units, exchangeDTO);
        calculateBuyingUnits(code, units, exchangeDTO);
        TransactionDTO transaction = new TransactionDTO(isAsx ? "ASX" : "CXA", code, value, units, new Date(), "Buying");
        exchangeDTO.addTransaction(transaction);
    }

    private void calculateSelling(Boolean isAsx, String code, int units, ExchangeDTO exchangeDTO) {
        Double value = calculateIncome(false, code, units, exchangeDTO);
        calculateSellingUnits(code, units, exchangeDTO);
        TransactionDTO transaction = new TransactionDTO(isAsx ? "ASX" : "CXA", code, value, units, new Date(), "Selling");
        exchangeDTO.addTransaction(transaction);
    }

    private Double calculateIncome(Boolean isBuying, String code, int units, ExchangeDTO exchangeDTO) {
        Double income = exchangeDTO.getStockPrices().get(code) * units - stockExchange.getTradingCosts().doubleValue();
        Double totalIncome = isBuying ? exchangeDTO.getIncomeOfExchange().get(code) + income : exchangeDTO.getIncomeOfExchange().get(code) - income;
        exchangeDTO.getIncomeOfExchange().put(code, totalIncome);
        return income;
    }

    private void calculateBuyingUnits(String code, int units, ExchangeDTO exchangeDTO) {
        exchangeDTO.setStockUnitsOwnedByCode(code, exchangeDTO.getStockUnitsOwned().get(code) + units);
    }

    private void calculateSellingUnits(String code, int units, ExchangeDTO exchangeDTO) {
        exchangeDTO.setStockUnitsOwnedByCode(code, exchangeDTO.getStockUnitsOwned().get(code) - units);
    }

    private void reportIncomeAndUnits(ExchangeDTO exchangeDTO) {
        System.out.println("Income and units owned at the end of trading");
        for (Map.Entry entry : exchangeDTO.getIncomeOfExchange().entrySet()) {
            System.out.println("Code: " + entry.getKey() + " | " + entry.getValue() + "\t\t | " + exchangeDTO.getStockUnitsOwned().get(entry.getKey()));
        }
        System.out.println("");
        System.out.println("All transaction so far");
        for (TransactionDTO t : exchangeDTO.getTransactions()) {
            System.out.println("stockMarket: " + t.getStockMarket() + ", Code: " + t.getCode() + ", Value: " + t.getValue() +
                    ", Units: " + t.getUnits() + ", Date: " + t.getDate() + ", Transaction: " + t.getTransactionType());
        }
    }

    private void reportVolume() {
        Map<String, Integer>  volume = stockExchange.getOrderBookTotalVolume();
        for (Map.Entry entry : volume.entrySet()) {
            System.out.println("Code: " + entry.getKey() + " => " + entry.getValue());
        }
        System.out.println("");
    }

}