package au.com.livewirelabs.assignment;

import au.com.livewirelabs.assignment.dto.ExchangeDTO;

import java.io.IOException;

public interface ExchangeDataAccess {
    ExchangeDTO getExchange(Boolean isAsx) throws IOException;
    void setExchange(ExchangeDTO exchangeDTO, Boolean isAsx) throws IOException;
}
